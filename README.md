# Atlassian for VS Code
Atlassian for VS Code brings the functionality of Atlassian products to your favorite IDE!

This extension combines the power of Jira and Bitbucket to streamline the developer workflow within VS Code.

With Atlassian for VS Code you can create and view issues, start work on issues, create pull requests, do code reviews, start builds, get build statuses and more!

## Getting Started
* Make sure you have VS Code version 1.37.0 or above
* Download the extension from the marketplace
* Authenticate with Jira and/or Bitbucket from the 'Atlassian: Open Settings' page available in the command palette
* From the command palette, type 'Atlassian:' to see all of the extensions available commands

For more in-depth information, check out our [User Guide](https://confluence.atlassian.com/display/BITBUCKET/Getting+started+with+VS+Code)

## Features at a Glance

Here's a quick peek at a developer's workflow:

![dev workflow](https://bitbucket.org/atlassianlabs/atlascode/raw/master/.readme/dev-workflow.gif)

Reviewing with Bitbucket pull request features is a snap:

![review pr](https://bitbucket.org/atlassianlabs/atlascode/raw/master/.readme/review-pr.gif)

Got a burning issue you'd like to work on?

![start work](https://bitbucket.org/atlassianlabs/atlascode/raw/master/.readme/issue-start-work.gif)

Kick off your builds:

![builds](https://bitbucket.org/atlassianlabs/atlascode/raw/master/.readme/start-pipeline.gif)

Create that issue without breaking your stride:

![issue from todo](https://bitbucket.org/atlassianlabs/atlascode/raw/master/.readme/create-from-code-lens.gif)

...and lots more

## Everyone Has Issues...
Please refer to [our issue tracker for known issues](https://bitbucket.org/atlassianlabs/atlascode/issues) and please contribute if you encounter an issue yourself.

### Questions? Comments? Kudos?
Please use the in-app feedback form to tell us what you think!  It's available from the 'Atlassian: Open Settings' and 'Atlassian: Open Welcome' pages available in the command palette.

## Contributors

Pull requests, issues and comments welcome.

Please read our [Code of Conduct](CODE_OF_CONDUCT.md).

For pull requests:

* Follow the existing style
* Separate unrelated changes into multiple pull requests

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)
